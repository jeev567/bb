# get past performance data
cd ./_data/model/
rm -rf ./pp_merge_unique/
aws s3api get-object --bucket fotwoutput0001 --key _model_data/pp_merge_unique.zip pp_merge_unique.zip
unzip pp_merge_unique.zip
mv ./_output_system/pp_merge_unique .
rm -rf ./_output_system/
rm pp_merge_unique.zip
cd ../..

# get charts data
cd ./_data/model/
rm -rf ./charts_merge/
aws s3api get-object --bucket fotwoutput0001 --key _model_data/d.entry.merge.csv.zip d.entry.merge.csv.zip
unzip d.entry.merge.csv.zip
mv ./_output_system/charts_merge .
rm -rf ./_output_system/
rm d.entry.merge.csv.zip
cd ../..
