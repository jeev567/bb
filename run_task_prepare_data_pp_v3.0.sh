# run_task_prepare_data_pp_v3.0.sh
# Rscript task_prepare_data_pp_v3.0.R abt &

MAX_PROCESSES=2;
PROCESSES=0;

for item in $(ls -Sr ./_data/past-performance/_new/*.xml); do
  echo $item
  Rscript task_prepare_data_pp_v3.0.R $item &
  sleep 1
	let PROCESSES=PROCESSES+1
  if [ $((PROCESSES % MAX_PROCESSES)) -eq 0 ]; then
    	echo "wait $PROCESSES"
      wait
    fi
done  
wait

exit 0;
