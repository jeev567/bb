library(tidyverse)
library(lubridate)
rm(list=ls())
# d.entry <- read_csv ("d.entry.csv")
d.entry <- read_csv("_data/d.entry.merge.csv")

model_creation <- proc.time()

# removing troublesome data
d.entry <- unique(d.entry)

d.entry <- d.entry %>% filter(race_surface != TRUE)
d.entry <- d.entry %>% filter(race_distance_unit != 'F',race_distance_unit != 'Y' )
d.entry$horse_dam <- NULL
d.entry$horse_sire <- NULL
d.entry$horse_dam_sire <- NULL 

# date transfomrations and creating an ID
d.entry$race_date <- as.Date(d.entry$race_date)
d.entry$race_id <- paste(d.entry$race_date, d.entry$location_code, d.entry$race_number, sep = '_')
d.entry$year <- year(d.entry$race_date)
d.entry$month <- month(d.entry$race_date)

# arrage so lags are meaningful
d.entry <- d.entry %>% arrange(race_date, race_id, horse_post_position)

#create speed variable
d.entry <- d.entry %>% mutate(speed = race_distance / horse_finish_time_calc)

# market probability 

d.entry <- d.entry %>% mutate(
  market_prob = 1- (horse_dollar_odds/ (1+ horse_dollar_odds))
)


# create horse based lags  ----------------------------------------------------------------
#THIS IS REPLACING "HORSEY ATM

horse.lags <- d.entry %>% group_by( horse_name) %>% mutate(
  speed_minus_one= lag(speed),
  speed_minus_two= lag(speed,2),    
  speed_minus_three= lag(speed, 3),
  speed_minus_four= lag(speed, 4),
  speed_minus_five= lag(speed, 5),
  speed_minus_six= lag(speed, 6),
  speed_minus_seven= lag(speed, 7),
  speed_minus_eight= lag(speed, 8),
  speed_minus_nine= lag(speed, 9),
  speed_minus_ten= lag(speed, 10),
  speed_minus_eleven= lag(speed, 11),
  speed_minus_twelve= lag(speed, 12),
  speed_minus_thirteen= lag(speed, 13),
  speed_minus_fourteen= lag(speed, 14),
  speed_minus_fifteen= lag(speed, 15)
  ) %>%
  rowwise()%>%
 mutate( avg_speed = mean( c(speed_minus_one,
                             speed_minus_two,
                             speed_minus_three,
                             speed_minus_four,
                             speed_minus_five,
                             speed_minus_six,
                             speed_minus_seven,
                             speed_minus_eight,
                             speed_minus_nine,
                             speed_minus_ten,
                             speed_minus_eleven,
                             speed_minus_twelve,
                             speed_minus_thirteen,
                             speed_minus_fourteen,
                             speed_minus_fifteen), na.rm = TRUE),
         median_speed = median( c(speed_minus_one,
                             speed_minus_two,
                             speed_minus_three,
                             speed_minus_four,
                             speed_minus_five,
                             speed_minus_six,
                             speed_minus_seven,
                             speed_minus_eight,
                             speed_minus_nine,
                             speed_minus_ten,
                             speed_minus_eleven,
                             speed_minus_twelve,
                             speed_minus_thirteen,
                             speed_minus_fourteen,
                             speed_minus_fifteen), na.rm = TRUE),
         recent_speed = mean( c(speed_minus_one,
                                speed_minus_two,
                                speed_minus_three), na.rm = TRUE)) %>%
  select(horse_name,
         race_id,
         speed_minus_one,
         speed_minus_two,
         speed_minus_three,
         avg_speed,
         median_speed, 
         recent_speed) %>% rename(
    PP_speed_minus_one = speed_minus_one, 
    PP_speed_minus_two = speed_minus_two, 
    PP_speed_minus_three = speed_minus_three,
    PP_avg_speed = avg_speed,
    PP_median_speed = median_speed,
    PP_recent_speed = recent_speed
  )

# create jockey based lags  ----------------------------------------------------------------
#THIS IS REPLACING "JOCKEY" ATM
jockey.lags <- d.entry %>% group_by( jockey_lname_plus_key) %>% mutate(
  speed_minus_one= lag(speed),
  speed_minus_two= lag(speed,2),    
  speed_minus_three= lag(speed, 3),
  speed_minus_four= lag(speed, 4),
  speed_minus_five= lag(speed, 5),
  speed_minus_six= lag(speed, 6),
  speed_minus_seven= lag(speed, 7),
  speed_minus_eight= lag(speed, 8),
  speed_minus_nine= lag(speed, 9),
  speed_minus_ten= lag(speed, 10),
  speed_minus_eleven= lag(speed, 11),
  speed_minus_twelve= lag(speed, 12),
  speed_minus_thirteen= lag(speed, 13),
  speed_minus_fourteen= lag(speed, 14),
  speed_minus_fifteen= lag(speed, 15)
) %>%
  rowwise()%>%
  mutate( avg_speed = mean( c(speed_minus_one,
                              speed_minus_two,
                              speed_minus_three,
                              speed_minus_four,
                              speed_minus_five,
                              speed_minus_six,
                              speed_minus_seven,
                              speed_minus_eight,
                              speed_minus_nine,
                              speed_minus_ten,
                              speed_minus_eleven,
                              speed_minus_twelve,
                              speed_minus_thirteen,
                              speed_minus_fourteen,
                              speed_minus_fifteen), na.rm = TRUE),
          median_speed = median( c(speed_minus_one,
                                   speed_minus_two,
                                   speed_minus_three,
                                   speed_minus_four,
                                   speed_minus_five,
                                   speed_minus_six,
                                   speed_minus_seven,
                                   speed_minus_eight,
                                   speed_minus_nine,
                                   speed_minus_ten,
                                   speed_minus_eleven,
                                   speed_minus_twelve,
                                   speed_minus_thirteen,
                                   speed_minus_fourteen,
                                   speed_minus_fifteen), na.rm = TRUE),
          recent_speed = mean( c(speed_minus_one,
                                 speed_minus_two,
                                 speed_minus_three), na.rm = TRUE)) %>%
  select(jockey_lname_plus_key,
         race_id,
         speed_minus_one,
         speed_minus_two,
         speed_minus_three,
         avg_speed,
         median_speed, 
         recent_speed) %>% rename(
           J_PP_speed_minus_one = speed_minus_one, 
           J_PP_speed_minus_two = speed_minus_two, 
           J_PP_speed_minus_three = speed_minus_three,
           J_PP_avg_speed = avg_speed,
           J_PP_median_speed = median_speed,
           J_PP_recent_speed = recent_speed
         )

# Horseys ----------------------------------------------------------------

horsey <- d.entry %>% group_by (horse_name) %>%
  summarize(
    num_races = n(),
    avg_position = mean(horse_finish_position),
    best_position = min(horse_finish_position),
    worst_position = max(horse_finish_position),
    num_show = sum(horse_finish_position == 3 | horse_finish_position == 2 | horse_finish_position ==1),
    num_place = sum(horse_finish_position == 2 | horse_finish_position ==1),
    num_win = sum(horse_finish_position ==1),
    avg_weight = mean(horse_weight),
    avg_speed = mean(speed)
  ) %>% mutate (
    show_ratio = num_show / num_races,
    place_ratio = num_place / num_races,
    win_ratio = num_win / num_races
  )

# jockeys ----------------------------------------------------------------
# CURRENTLY NOT INCLUDED DUE TO FORWARD BIAS 
jockey <- d.entry %>% group_by (jockey_lname_plus_key) %>%
  summarize(
    num_races = n(),
    avg_position = mean(horse_finish_position),
    best_position = min(horse_finish_position),
    worst_position = max(horse_finish_position),
    num_show = sum(horse_finish_position == 3 | horse_finish_position == 2 | horse_finish_position ==1),
    num_place = sum(horse_finish_position == 2 | horse_finish_position ==1),
    num_win = sum(horse_finish_position ==1),
    avg_weight = mean(horse_weight),
    avg_speed = mean(speed)
  ) %>% mutate (
    show_ratio = num_show / num_races,
    place_ratio = num_place / num_races,
    win_ratio = num_win / num_races
  )


# DISTANCE ----------------------------------------------------------------
# INCLUDED EVEN THOUGH THERE IS FORWARD BIAS
distance <- d.entry %>% group_by (race_distance) %>%
  summarize(
    num_races_dist = n(),
    avg_race_win_time_dist = mean(race_win_time),
    avg_race_show_time_dist = mean(horse_finish_time_calc[which(horse_finish_position== 3| horse_finish_position ==2 | horse_finish_position ==1)]),
    min_race_win_time_dist= min(race_win_time),
    min_race_show_time_dist = min(horse_finish_time_calc[which(horse_finish_position== 3| horse_finish_position ==2 | horse_finish_position ==1)]),
    med_race_win_time_dist= median(race_win_time),
    med_race_show_time_dist = median(horse_finish_time_calc[which(horse_finish_position== 3| horse_finish_position ==2 | horse_finish_position ==1)]),
    max_race_win_time_dist = max(race_win_time),
    max_race_show_time_dist = max(horse_finish_time_calc[which(horse_finish_position== 3| horse_finish_position ==2 | horse_finish_position ==1)])
  ) 


# Horesey_distance ----------------------------------------------------------------
# NOT CURRENTLY IN USE  DO TO FORWARD BIAS
horsey_distance <- d.entry %>% group_by(horse_name, race_distance ) %>%
                                          summarize( 
                                            count_hd = n(),
                                            min_time_HD =  min(horse_finish_time_calc),
                                            max_time_HD =  max(horse_finish_time_calc),
                                            avg_time_HD=  mean(horse_finish_time_calc)
                                            #,
                                           # days_since_last_race_HD = as.numeric(curr.date - last(race_date))
                                            )
# calculate horse ranking within
d.entry<- d.entry %>% group_by(race_id) %>% mutate(
  ranking = rank(horse_dollar_odds, ties.method = 'first')) %>% ungroup()  
# race_pp----------------------------------------------------------------


                      
# horse.lags  ----------------------------------------------------------------

horse_position <- d.entry %>% select(race_id, horse_post_position, horse_name,ranking)

horse.lags <- horse.lags %>% left_join (horse_position)
# race_id_PP_top_5 ----------------------------------------------------------------

# means are just to deal with people who are tied for 2nd position (as an example)
# TODO(MB): This part is the slowest commponent of the script. refactor please.

ptm <- proc.time()

race_id_PP <- horse.lags %>% group_by(race_id) %>% summarize(
  rank1_PP_speed = mean(PP_avg_speed[ranking==1]),
  rank2_PP_speed = mean(PP_avg_speed[ranking==2]),
  rank3_PP_speed = mean(PP_avg_speed[ranking==3]),
  rank4_PP_speed = mean(PP_avg_speed[ranking==4]),
  rank5_PP_speed = mean(PP_avg_speed[ranking==5]),
  rank6_PP_speed = mean(PP_avg_speed[ranking==6]),
  rank7_PP_speed = mean(PP_avg_speed[ranking==7]),
  rank8_PP_speed = mean(PP_avg_speed[ranking==8]),
  rank9_PP_speed = mean(PP_avg_speed[ranking==9]),
  rank10_PP_speed = mean(PP_avg_speed[ranking==10]),
  rank11_PP_speed = mean(PP_avg_speed[ranking==11]),
  rank12_PP_speed = mean(PP_avg_speed[ranking==12]),
  rank1_PP_median_speed = mean(PP_median_speed[ranking==1]),
  rank2_PP_median_speed = mean(PP_median_speed[ranking==2]),
  rank3_PP_median_speed = mean(PP_median_speed[ranking==3]),
  rank4_PP_median_speed = mean(PP_median_speed[ranking==4]),
  rank5_PP_median_speed = mean(PP_median_speed[ranking==5]),
  rank6_PP_median_speed = mean(PP_median_speed[ranking==6]),
  rank7_PP_median_speed = mean(PP_median_speed[ranking==7]),
  rank8_PP_median_speed = mean(PP_median_speed[ranking==8]),
  rank9_PP_median_speed = mean(PP_median_speed[ranking==9]),
  rank10_PP_median_speed = mean(PP_median_speed[ranking==10]),
  rank11_PP_median_speed = mean(PP_median_speed[ranking==11]),
  rank12_PP_median_speed = mean(PP_median_speed[ranking==12]),
  rank1_PP_recent_speed = mean(PP_recent_speed[ranking==1]),
  rank2_PP_recent_speed = mean(PP_recent_speed[ranking==2]),
  rank3_PP_recent_speed = mean(PP_recent_speed[ranking==3]),
  rank4_PP_recent_speed = mean(PP_recent_speed[ranking==4]),
  rank5_PP_recent_speed = mean(PP_recent_speed[ranking==5]),
  rank6_PP_recent_speed = mean(PP_recent_speed[ranking==6]),
  rank7_PP_recent_speed = mean(PP_recent_speed[ranking==7]),
  rank8_PP_recent_speed = mean(PP_recent_speed[ranking==8]),
  rank9_PP_recent_speed = mean(PP_recent_speed[ranking==9]),
  rank10_PP_recent_speed = mean(PP_recent_speed[ranking==10]),
  rank11_PP_recent_speed = mean(PP_recent_speed[ranking==11]),
  rank12_PP_recent_speed = mean(PP_recent_speed[ranking==12]),
  rank1_PP_last_finish = mean(PP_speed_minus_one[ranking==1]),
  rank2_PP_last_finish = mean(PP_speed_minus_one[ranking==2]),
  rank3_PP_last_finish = mean(PP_speed_minus_one[ranking==3]),
  rank4_PP_last_finish = mean(PP_speed_minus_one[ranking==4]),
  rank5_PP_last_finish = mean(PP_speed_minus_one[ranking==5]),
  rank6_PP_last_finish = mean(PP_speed_minus_one[ranking==6]),
  rank7_PP_last_finish = mean(PP_speed_minus_one[ranking==7]),
  rank8_PP_last_finish = mean(PP_speed_minus_one[ranking==8]),
  rank9_PP_last_finish = mean(PP_speed_minus_one[ranking==9]),
  rank10_PP_last_finish = mean(PP_speed_minus_one[ranking==10]),
  rank11_PP_last_finish = mean(PP_speed_minus_one[ranking==11]),
  rank12_PP_last_finish = mean(PP_speed_minus_one[ranking==12])
)

proc.time() - ptm

# data (main prediction file)  ----------------------------------------------------------------

data <- d.entry %>% select (race_id, 
                            race_date,
                            jockey_lname_plus_key,
                            market_prob,
                            horse_dollar_odds,
                                  horse_name, 
                                  horse_weight,
                                  horse_sex_code, 
                                  horse_age, 
                                  # horse_breed, # only one factor
                                  location_code,
                                  race_distance,
                                  race_surface,
                                  race_track_condition,
                                  race_weather_condition,
                                  horse_post_position,
                                  horse_finish_position)
                              

# Join in previous performance for all horses, by race_id
data <- data %>% left_join(race_id_PP)

# Join in summary statistics for the distance to the race (things like how long it takes to run the race on average)
data <- data %>% left_join(distance)

# Join in the previous race information for the horse being predicted
data <- data %>% left_join(horse.lags)

# Join in the previous race information for the jockey on the horse being predicted

data <- data %>% left_join(jockey.lags)

# creation of the target variable
data$is_show <- ifelse(data$horse_finish_position ==1 |data$horse_finish_position ==2 |data$horse_finish_position ==1 , 1 ,0)

# location_code_factor <- factor(data$location_code)
# location_code_vars <- model.matrix(~location_code_factor)

# horse_breed_factor <- factor(data$horse_breed)
# horse_breed_vars <- model.matrix(~horse_breed_factor)

horse_sex_code_factor <-  factor(data$horse_sex_code)
horse_sex_code_vars = model.matrix(~horse_sex_code_factor)


race_track_condition_factor <- factor(data$race_track_condition)
race_track_condition_vars <- model.matrix(~race_track_condition_factor)

race_weather_condition_factor <- factor(data$race_weather_condition)
race_weather_condition_vars <- model.matrix(~race_weather_condition_factor)

race_surface_factor <- factor(data$race_surface)
race_surface_vars <- model.matrix(~race_surface_factor)

data<- cbind(data, #location_code_vars,
              # horse_breed_vars,
              horse_sex_code_vars,
              race_track_condition_vars,
              race_weather_condition_vars,
              race_surface_vars)

data$'(Intercept)' <- NULL
data$'(Intercept)' <- NULL
data$'(Intercept)' <- NULL
data$'(Intercept)' <- NULL
data$'(Intercept)' <- NULL

rm(location_code_factor,
  location_code_vars,
   # horse_breed_vars,
  # horse_breed_factor,
   horse_sex_code_vars,
  horse_sex_code_factor,
   race_track_condition_vars,
  race_track_condition_factor,
   race_weather_condition_vars,
  race_weather_condition_factor,
   race_surface_factor,
  race_surface_vars)

rm(horsey,
   horsey_distance,
   horse_position,
   horse.lags,
   jockey,
   jockey.lags,
   race_id_PP,
   distance
   )

gc()


# Train / Test datasets ---------------------------------------------------
# remove first YEAR of data. 

# filter takes a a long amount of time too

curr.date <- as.Date('2016-11-05')  # // Breeders Cup 2016

date.test.end <- curr.date -1
date.test.start <- curr.date -8
  
date.train.end <-curr.date - 9 
date.train.start <-curr.date - 1104 # min date is really "2014-01-01"
data2 <- data %>% filter( race_date >= date.train.start, race_date <=date.train.end)

train <- as.data.frame(data2)
train$race_date <- NULL
train$race_id <- NULL
# train$horse_dollar_odds <- NULL # ODDS AT TIME OF BET
train$horse_finish_position <- NULL
train$horse_finish_time_calc <- NULL
train$horse_name <- NULL
train$location_code <- NULL
train$horse_sex_code <- NULL
train$race_track_condition <- NULL
train$race_weather_condition <- NULL
train$race_surface <- NULL
train$jockey_lname_plus_key <- NULL


# Model -------------------------------------------------------------------
library(xgboost)

params <- list(
  "objective"           = "binary:logistic",
  "eval_metric"         = 'error',# "logloss",
  "eta"                 = .02, # trying degault rate#.05, #0.1,  adjusting learning rate
  "max_depth"           = 6,
  "min_child_weight"    = 10,
  "gamma"               = 0.70,
  "subsample"           = 0.80,
  "colsample_bytree"    = 0.95,
  "alpha"               = 2e-05,
  "lambda"              = 10#, 
  #"tree_method"         = "exact"
)

subtrain <- train #%>% sample_frac(0.1)
X <- xgb.DMatrix(as.matrix(subtrain %>% select(-is_show, -ranking, -market_prob, -horse_dollar_odds)), label = subtrain$is_show)
model <- xgboost(data = X, params = params, nrounds = 500) #2000

importance <- xgb.importance(colnames(X), model = model)
xgb.ggplot.importance(importance)


# save model for later use------------------------------------------------------------

xgb.save(model, "oct_20.model")
# create test set-------------------------------------------------------------

data3 <- data %>% filter( race_date >= date.test.start, race_date <=date.test.end)#, race_date <= '2017-4-02')
# data3 <- data %>% filter(race_date >= '2016-01-01', race_date <= '2016-12-31') # to look at prediction distributions

test <- as.data.frame(data3)

test$race_date <- NULL
# train$horse_dollar_odds <- NULL # ODDS AT TIME OF BET
# test$horse_finish_position 
test$horse_finish_time_calc <- NULL
test$location_code <- NULL
test$horse_sex_code <- NULL
test$race_track_condition <- NULL
test$race_weather_condition <- NULL
test$race_surface <- NULL
test$jockey_lname_plus_key <- NULL



model_creation - proc.time()

# Apply model -------------------------------------------------------------


X <- xgb.DMatrix(as.matrix(test %>% select(-is_show, -horse_name, - race_id, -horse_finish_position, -ranking, -market_prob, -horse_dollar_odds)))
test$predicted <- predict(model, X)


##### 

bet.strat.df <- test %>% select(horse_name,race_id, market_prob, horse_dollar_odds, horse_post_position, horse_finish_position, predicted)
bet.strat.df <-bet.strat.df %>% mutate(
  two.dollar.win = ifelse(horse_finish_position ==1, ((horse_dollar_odds*2) +2), 0)
)

bet.strat.df2 <- bet.strat.df %>% group_by(race_id) %>% mutate(
  ranking = rank(horse_dollar_odds, ties.method = 'first'),
  ranking.predicted  =rank(-predicted, ties.method = 'first') ) %>% ungroup()  


bet.strat.df2 <- bet.strat.df2 %>% mutate(
  diff.predicted.rank = ranking.predicted - ranking,
  diff.predicted.market.prob = predicted - market_prob
)


bet.strat.df2 <- bet.strat.df2 %>%
  mutate(
    winnings_A= ifelse((diff.predicted.rank<0 & ranking.predicted <= 3), (two.dollar.win-2), 0 ),
    winnings_B= ifelse((ranking==1 & ranking.predicted ==1), (two.dollar.win-2), 0 ),
    winnings_C= ifelse((ranking==2 & ranking.predicted ==2), (two.dollar.win-2), 0 ),
    winnings_D= ifelse((ranking==1 & ranking.predicted <=3), (two.dollar.win-2), 0 ),
    winnings_E= ifelse((ranking==2 & ranking.predicted <=3), (two.dollar.win-2), 0 ),
    winnings_F= ifelse((ranking==3 & ranking.predicted <=3), (two.dollar.win-2), 0 )
  ) %>% ungroup() %>% mutate(
    total_winnings = (winnings_A + winnings_B + winnings_C +winnings_D +  winnings_E +winnings_F  )
  )


# HOLDOUT

# create holdout set-------------------------------------------------------------

data4 <- data %>% filter( race_date == curr.date)#, race_date <= '2017-4-02')
# data3 <- data %>% filter(race_date >= '2016-01-01', race_date <= '2016-12-31') # to look at prediction distributions

holdout <- as.data.frame(data4)

holdout$race_date <- NULL
# train$horse_dollar_odds <- NULL # ODDS AT TIME OF BET
# test$horse_finish_position 
holdout$horse_finish_time_calc <- NULL
holdout$location_code <- NULL
holdout$horse_sex_code <- NULL
holdout$race_track_condition <- NULL
holdout$race_weather_condition <- NULL
holdout$race_surface <- NULL
holdout$jockey_lname_plus_key <- NULL



model_creation - proc.time()

# Apply model -------------------------------------------------------------


X <- xgb.DMatrix(as.matrix(holdout %>% select(-is_show, -horse_name, - race_id, -horse_finish_position, -ranking, -market_prob, -horse_dollar_odds)))
holdout$predicted <- predict(model, X)

bet.strat.holdout <- holdout %>% select(horse_name,race_id, market_prob, horse_dollar_odds, horse_post_position, horse_finish_position, predicted)
bet.strat.holdout <-bet.strat.holdout %>% mutate(
  two.dollar.win = ifelse(horse_finish_position ==1, ((horse_dollar_odds*2) +2), 0)
)

bet.strat.holdout2 <- bet.strat.holdout %>% group_by(race_id) %>% mutate(
  ranking = rank(horse_dollar_odds, ties.method = 'first'),
  ranking.predicted  =rank(-predicted, ties.method = 'first') ) %>% ungroup()  


bet.strat.holdout2 <- bet.strat.holdout2 %>% mutate(
  diff.predicted.rank = ranking.predicted - ranking,
  diff.predicted.market.prob = predicted - market_prob
)


bet.strat.holdout2 <- bet.strat.holdout2 %>%
  mutate(
    winnings_A= ifelse((diff.predicted.rank<0 & ranking.predicted <= 3), (two.dollar.win-2), 0 ),
    winnings_B= ifelse((ranking==1 & ranking.predicted ==1), (two.dollar.win-2), 0 ),
    winnings_C= ifelse((ranking==2 & ranking.predicted ==2), (two.dollar.win-2), 0 ),
    winnings_D= ifelse((ranking==1 & ranking.predicted <=3), (two.dollar.win-2), 0 ),
    winnings_E= ifelse((ranking==2 & ranking.predicted <=3), (two.dollar.win-2), 0 ),
    winnings_F= ifelse((ranking==3 & ranking.predicted <=3), (two.dollar.win-2), 0 )
  ) %>% ungroup()












names(test)

test2<- test %>% group_by(race_id ) %>% mutate(
  ranking_prediction = rank(-predicted, ties.method = 'first')) %>% ungroup()  

# TESTING STRATEGY FOR A DAY
bet_sheet_2017_10_18_BEL <- test2 %>% filter(grepl("2017-10-18_BEL", race_id)) %>%
  select (race_id, ranking, predicted, ranking_prediction, horse_post_position, horse_name, market_prob)  %>% arrange (race_id, desc(predicted)) %>% mutate(
    money_odds = ((market_prob - 1) / -market_prob),
    one_EV = (predicted *1) *money_odds
  ) %>% arrange(desc(one_EV)) %>% print(n=100)


write.csv(bet_sheet_2017_10_18_BEL, "bet_sheet_2017_10_18_BEL.csv")








## STOP HERE

 a <- test2 %>% group_by(ranking_prediction, ranking) %>% summarize( first_place = sum(horse_finish_position==1),
                                                      second_place = sum(horse_finish_position==2),
                                                      third_place = sum(horse_finish_position==3), 
                                                      not_show = sum(horse_finish_position >3),
                                                      show = sum(horse_finish_position<= 3),
                                                      place = sum(horse_finish_position<= 2),
                                                      not_place = sum(horse_finish_position> 2)) %>% mutate(
                                                        show_ratio = show/ (show +not_show),
                                                        place_ratio =  place / (place +not_place ),
                                                        first_ratio =  first_place /(show +not_show),
                                                        second_ratio =  second_place /(show +not_show),
                                                        third_ratio =  third_place /(show +not_show)
                                                      ) %>% print (n = 32)
                                                      
test2 %>% group_by(ranking) %>% summarize( first_place = sum(horse_finish_position==1),
                                                      second_place = sum(horse_finish_position==2),
                                                      third_place = sum(horse_finish_position==3), 
                                                      not_show = sum(horse_finish_position >3),
                                                      show = sum(horse_finish_position<= 3)) %>% mutate(
                                                        ratio = show/ (show +not_show),
                                                        first_ratio =  first_place /(show +not_show),
                                                        second_ratio =  second_place /(show +not_show),
                                                        third_ratio =  third_place /(show +not_show)
                                                      )
test2 %>% group_by(ranking_prediction) %>% summarize( first_place = sum(horse_finish_position==1),
                                           second_place = sum(horse_finish_position==2),
                                           third_place = sum(horse_finish_position==3), 
                                           not_show = sum(horse_finish_position >3),
                                           show = sum(horse_finish_position<= 3)) %>% mutate(
                                             ratio = show/ (show +not_show)
                                           )



training.set.summary.statistics <- d.entry %>% group_by(ranking) %>% summarize( first_place = sum(horse_finish_position==1),
                                             second_place = sum(horse_finish_position==2),
                                             third_place = sum(horse_finish_position==3),
                                             not_show = sum(horse_finish_position >3),
                                             show = sum(horse_finish_position<= 3),
                                             place = sum(horse_finish_position<= 2),
                                             not_place = sum(horse_finish_position >2)) %>% mutate(
                                               ratio = show/ (show +not_show),
                                               place_ratio = place /(place + not_place))


bet_sheet_2017_01_21_AQU<- test2 %>% filter(grepl("2017-01-21_AQU", race_id)) %>%
  select (race_id, ranking, predicted, ranking_prediction, horse_post_position, horse_name) %>% arrange(race_id) %>% mutate (
    bet = ifelse((ranking <= 3  & ranking_prediction <= 3), "Y", "N"))%>% filter( bet == "Y")

test2 %>% filter( race_id == '2017-01-21_AQU_1') %>% select( race_id, ranking, predicted, ranking_prediction, horse_post_position, horse_name)%>% arrange(desc(ranking_prediction))
