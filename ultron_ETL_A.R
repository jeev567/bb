#################################################################################################################
#
# ultron_ETL_A.R
# Author:                     Carmine Genovese
# Last Revision:              Feb 12th 2018
# Executive Summary:          Splititng up the code into managable chunks. This first section is the basic ETL needed
#                             to get to the prediction file. 
#
# Outsanding Major TODO:      
#
# TODO(CG):

#
#
#################################################################################################################

# Configuration ---------------------------------------------------------------


library(tidyverse)
library(lubridate)
library(data.table)
#library(tidyquant)
#library(TTR)
library(zoo)

rm(list=ls())

job.date <- Sys.Date()


# Load Data ----------------------------------------------------------------------------------------------------------------------------

d.entry <- read_csv("_output_system/charts_merge/d.entry.merge.csv")
race.entry.df <- fread("_output_system/pp_merge_unique/pp_race_entry_merge_unique.csv")
race.stuff.df <- fread("_output_system/pp_merge_unique/pp_race_merge_unique.csv")
workout.df <- fread("_output_system/pp_merge_unique/pp_workout_merge_unique.csv")
race.df <- fread("_output_system/pp_merge_unique/pp_past_performance_merge_unique.csv")

model_creation <- proc.time()
# transform Race Id - race stuff

race.stuff.df$track <- as.factor(race.stuff.df$track)
race.stuff.df$race_date <- as.Date(race.stuff.df$race_date, format = '%Y%m%d')
race.stuff.df$race <- as.numeric(race.stuff.df$race)
race.stuff.df$race_id <- paste(race.stuff.df$race_date, race.stuff.df$track, race.stuff.df$race, sep = '_')


# TRANSFORM //  fix race.entry.df information ------------------------------------------------

race.entry.df$track <- as.factor(race.entry.df$track)
race.entry.df$race_date <- as.Date(race.entry.df$race_date, format = '%Y%m%d')
race.entry.df$race <- as.numeric(race.entry.df$race)
race.entry.df$race_id <- paste(race.entry.df$race_date, race.entry.df$track, race.entry.df$race, sep = '_')
race.entry.df$pp <- as.numeric(race.entry.df$pp)
race.entry.df$todays_cls  <- as.numeric(race.entry.df$todays_cls)
# avg_spd_sd  #26k NA
# ave_cl_sd # 26k NA
# hi_spd_sd # 26k NA
race.entry.df$foal_date <- as.Date(race.entry.df$foal_date, format = '%Y%m%d')
race.entry.df$pstyerl <- as.numeric(race.entry.df$pstyerl)
race.entry.df$pstymid <- as.numeric(race.entry.df$pstymid)
race.entry.df$pstynum <- as.numeric(race.entry.df$pstynum)
race.entry.df$pstyoff <- as.numeric(race.entry.df$pstyoff)
race.entry.df$psprstyerl <- as.numeric(race.entry.df$psprstyerl)
race.entry.df$psprstymid <- as.numeric(race.entry.df$psprstymid)
race.entry.df$psprstyfin <- as.numeric(race.entry.df$psprstyfin)
race.entry.df$psprstynum <- as.numeric(race.entry.df$psprstynum)
race.entry.df$psprstyoff <- as.numeric(race.entry.df$psprstyoff)
race.entry.df$prtestyerl <- as.numeric(race.entry.df$prtestyerl)
race.entry.df$prtestymid <- as.numeric(race.entry.df$prtestymid)
race.entry.df$prtestyfin <- as.numeric(race.entry.df$prtestyfin)
race.entry.df$prtestynum <- as.numeric(race.entry.df$prtestynum)
race.entry.df$prtestyoff <- as.numeric(race.entry.df$prtestyoff)
race.entry.df$pallstyerl <- as.numeric(race.entry.df$pallstyerl)
race.entry.df$pallstymid <- as.numeric(race.entry.df$pallstymid)
race.entry.df$pallstyfin <- as.numeric(race.entry.df$pallstyfin)
race.entry.df$pallstynum <- as.numeric(race.entry.df$pallstynum)
race.entry.df$pallstyoff <- as.numeric(race.entry.df$pallstyoff)
race.entry.df$pfigerl <- as.numeric(race.entry.df$pfigerl)
race.entry.df$pfigmid <- as.numeric(race.entry.df$pfigmid)
race.entry.df$pfigfin <- as.numeric(race.entry.df$pfigfin)
race.entry.df$pfignum <- as.numeric(race.entry.df$pfignum)
race.entry.df$pfigoff <- as.numeric(race.entry.df$pfigoff)
race.entry.df$psprfigerl <- as.numeric(race.entry.df$psprfigerl)
race.entry.df$psprfigmid <- as.numeric(race.entry.df$psprfigmid)
race.entry.df$psprfigfin <- as.numeric(race.entry.df$psprfigfin)
race.entry.df$psprfignum <- as.numeric(race.entry.df$psprfignum)
race.entry.df$psprfigoff <- as.numeric(race.entry.df$psprfigoff)
race.entry.df$prtefigerl <- as.numeric(race.entry.df$prtefigerl)
race.entry.df$prtefigmid <- as.numeric(race.entry.df$prtefigmid)
race.entry.df$prtefigfin <- as.numeric(race.entry.df$prtefigfin)
race.entry.df$prtefignum <- as.numeric(race.entry.df$prtefignum)
race.entry.df$prtefigoff <- as.numeric(race.entry.df$prtefigoff)
race.entry.df$pfigoff <- as.numeric(race.entry.df$pfigoff)
race.entry.df$pallfigerl <- as.numeric(race.entry.df$pallfigerl)
race.entry.df$pallfigmid <- as.numeric(race.entry.df$pallfigmid)
race.entry.df$pallfigfin <- as.numeric(race.entry.df$pallfigfin)
race.entry.df$pallfignum <- as.numeric(race.entry.df$pallfignum)
race.entry.df$pallfigoff <- as.numeric(race.entry.df$pallfigoff)
race.entry.df$tmmark <- as.factor(race.entry.df$tmmark)
race.entry.df$av_pur_val <- as.numeric(race.entry.df$av_pur_val)
race.entry.df$weight <- as.numeric(race.entry.df$weight)
race.entry.df$wght_shift <- as.numeric(race.entry.df$wght_shift)
race.entry.df$cldate <- as.Date(race.entry.df$cldate, format = "%Y-%m-%d")
race.entry.df$price <- as.numeric(race.entry.df$price)
race.entry.df$sex <- as.factor(race.entry.df$sex)
race.entry.df$power <- as.numeric(race.entry.df$power)
race.entry.df$med <- as.factor(race.entry.df$med)
race.entry.df$equip <- as.factor(race.entry.df$equip)

# TRANSFORM //  create rebate infromation------------------------------------------------

# rebate.info.df<- read_tsv("rebates.txt")
# names(rebate.info.df) <- c("track_long",
#                            "pool",
#                            "rebate")
rebate.info.df<- read_csv("./_config_system/Useable_Rebate_List_by_Track.csv")

rebate.info.df$reward <-as.numeric(sub("%", "", rebate.info.df$reward))
rebate.info.df$reward <- rebate.info.df$reward /100


# by putting this into vector it runs 1000x faster

morning.odds.vector <- vector(mode="numeric", length=nrow(race.entry.df))
morning.odds.vector.text <- vector(mode="character", length=nrow(race.entry.df))
morning.odds.vector.text <- race.entry.df$morn_odds

for (i in  1:length(morning.odds.vector )){
  if(morning.odds.vector.text[i]== ""){
    morning.odds.vector [i] <-0
  } else{
  morning.odds.vector [i] <- eval(parse(text = morning.odds.vector.text[i]))
  print(i)
  }
}

race.entry.df$morn_odds_numeric <- morning.odds.vector
race.entry.df$wh_foaled <- as.factor(race.entry.df$wh_foaled)
race.entry.df$ae_flag <- as.factor(race.entry.df$ae_flag)
race.entry.df$color <- as.factor(race.entry.df$color)
race.entry.df$power_symb <- as.factor(race.entry.df$power_symb)
race.entry.df$horse_comm <- as.factor(race.entry.df$horse_comm)
race.entry.df$breed_type <- as.factor(race.entry.df$breed_type)
race.entry.df$lst_salena <- as.factor(race.entry.df$lst_salena)
race.entry.df$lst_salepr <- as.numeric(race.entry.df$lst_salepr)
race.entry.df$lst_saleda <- as.numeric(race.entry.df$lst_saleda)
race.entry.df$claimprice <- as.numeric(race.entry.df$claimprice)
race.entry.df$avgspd <- as.numeric(race.entry.df$avgspd)
race.entry.df$avgcls <- as.numeric(race.entry.df$avgcls)
race.entry.df$apprweight <- as.numeric(race.entry.df$apprweight)
race.entry.df$horse_name <- gsub("\"", "", (gsub("'", "", toupper(race.entry.df$horse_name))))



# TRANSFORM //  fix race_distance_unit and race_surface "FALSE" == "F" and "TRUE" == "T" ------------------------------------------------
d.entry$race_distance_unit <- ifelse(d.entry$race_distance_unit == "FALSE", "F"
                                     , d.entry$race_distance_unit)
d.entry$race_surface <- ifelse(d.entry$race_surface == "TRUE", "T"
                               , d.entry$race_surface)

# TRANSFORM // Removing troublesome data ------------------------------------------------------------------------------------------------


d.entry <- unique(d.entry)
# going to keep ion the  different race distances for now. This is something that most likely doesnt belong in ETL A

#d.entry <- d.entry %>% filter(race_distance_unit == 'F')


d.entry$horse_dam <- NULL
d.entry$horse_sire <- NULL
d.entry$horse_dam_sire <- NULL 
d.entry$horse_dam <- NULL
d.entry$horse_sire <- NULL
d.entry$horse_dam_sire <- NULL 

# TRANSFORM // Creating an ID  on CHARTS data--------------------------------------------------------------------------------------------
d.entry$race_date <- as.Date(d.entry$race_date)
d.entry$race_id <- paste(d.entry$race_date, d.entry$location_code, d.entry$race_number, sep = '_')
d.entry$year <- year(d.entry$race_date)
d.entry$month <- month(d.entry$race_date)

# removing year filter... no reason to do this in ETL - A... will be handled in the modeling phase
# d.entry <- d.entry %>% filter (year >= 2016)


d.entry$horse_name <- gsub("\"", "", (gsub("'", "", toupper(d.entry$horse_name))))

# Transform // Create ID in PP data -----------------------------------------------------------------------------------------------------
race.df$racedate<- as.Date(race.df$racedate, format = "%Y%m%d")

#remove obserations that include an NA in the race date
race.df<- race.df[!(is.na(race.df$racedate)), ]

race.df <- race.df %>% mutate(
  race_id =  paste(racedate, trackcode, racenumber, sep = '_')
)

# Transform // Filter down to PP data needed --------------------------------------------------------------------------------------------

# There is a more esxtensive list that has more potentailly valuable information
# however we are currently just grabbing stuff for split information and fliters

race.filtered.df <- race.df %>% select(
  race_id,
  horse_name,
  horsetime1,
  horsetime2,
  horsetimes,
  horsetimef,
  leadertime,
  leadertim2,
  leadertim3,
  leadertim4,
  racebreed,
  purse,
  trackcondi,
  # runup_dist,# some variance here
  #rail_dist, # some variance here
  lenback1,
  lenback2,
  lenbackstr,
  lenbackfin,
  weightcarr,
  agerestric)


# Transform // Arrage so lags are meaningful --------------------------------------------------------------------------------------------
d.entry <- d.entry %>% arrange(race_date, race_id, horse_post_position)

# Transform // Filter CHART data down  --------------------------------------------------------------------------------------------------

d.entry <- d.entry %>% select (race_id,
                               race_date,
                               horse_dollar_odds,
                               horse_name,
                               horse_weight,
                               race_distance,
                               race_surface,
                               horse_post_position,
                               horse_finish_position,
                               horse_finish_time_calc,
                               win_payoff,
                               place_payoff,
                               show_payoff,
                               show_payoff2)
# Transform // Upper case CHARTS name to match with PP data  ----------------------------------------------------------------------------
d.entry <- d.entry %>% mutate(
  horse_name = toupper(horse_name)
)


# Transform // Fix data types from PP file-----------------------------------------------------------------------------------------------

race.filtered.df$horsetime1 <- as.numeric(race.filtered.df$horsetime1)
race.filtered.df$horsetime2 <- as.numeric(race.filtered.df$horsetime2)
race.filtered.df$horsetimes <- as.numeric(race.filtered.df$horsetimes)
race.filtered.df$horsetimef <- as.numeric(race.filtered.df$horsetimef)
race.filtered.df$leadertime <- as.numeric(race.filtered.df$leadertime)
race.filtered.df$leadertim2 <- as.numeric(race.filtered.df$leadertim2)
race.filtered.df$leadertim3 <- as.numeric(race.filtered.df$leadertim3)
race.filtered.df$leadertim4 <- as.numeric(race.filtered.df$leadertim4)
race.filtered.df$purse<- as.numeric(race.filtered.df$purse)

# Transform // Remove any time record with -99 ------------------------------------------------------------------------------------------

race.filtered.df <- race.filtered.df %>% filter(
  horsetime1 != -99,
  horsetime2 != -99, 
  horsetimes != -99,
  horsetimef != -99
)

# Transform // Reducde PP to unique records ---------------------------------------------------------------------------------------------

race.filtered.df <- unique(race.filtered.df)

# Transform // Merge in past performance data into chart data  --------------------------------------------------------------------------

d.charts.tmp <- d.entry
d.pp.tmp <- race.entry.df

d.pp.tmp.future <- d.pp.tmp %>%
  filter(d.pp.tmp$race_date >= job.date)

race.stuff.df.small <- race.stuff.df %>%
  select(race_date, race_id, distance) %>%
  rename(race_distance_lkp = distance)

d.pp.tmp.future.u1 <- d.pp.tmp.future %>%
  left_join(race.stuff.df.small)
rm(race.stuff.df.small, d.pp.tmp.future)

d.pp.tmp.future.u1$race_distance_lkp <- as.numeric(
  d.pp.tmp.future.u1$race_distance_lkp)

d.charts.pp.tmp <- d.charts.tmp %>%
  left_join(d.pp.tmp)
rm(d.charts.tmp, d.pp.tmp)

d.charts.pp.tmp.u1 <- d.charts.pp.tmp %>%
  inner_join(race.filtered.df)
rm(d.charts.pp.tmp)
d.charts.pp.tmp.u1$race_distance_lkp <- as.numeric(d.charts.pp.tmp.u1$race_distance)

d.pp.tmp.future.u2 <- d.pp.tmp.future.u1 %>%
  left_join(d.charts.pp.tmp.u1)
rm(d.pp.tmp.future.u1)

d.charts.pp.tmp.u2 <- rbind(d.charts.pp.tmp.u1, d.pp.tmp.future.u2)
rm(d.charts.pp.tmp.u1, d.pp.tmp.future.u2)

d.charts.pp.tmp.u2$race_distance <- d.charts.pp.tmp.u2$race_distance_lkp
d.charts.pp.tmp.u2$race_distance_lkp <- NULL

d.entry <- d.charts.pp.tmp.u2 

# Transform // Look for races that still have entries with multiple records, remove these races entirerly  ------------------------------

filter.df <- d.entry %>% group_by(race_id, horse_name) %>% summarize(count = n()) 
filter.df <- filter.df %>% filter(count >1) %>% select(race_id)
d.entry<- d.entry %>% filter(!race_id %in% filter.df$race_id)

# LOAD + TRANSFORM // Grabbing potential races for prediction ---------------------------------------------------------------------------
# look to old records for this. 


# Transform // create a speed variable by distance
d.entry <- d.entry %>% mutate(speed = race_distance / horsetimef)

# join in the race entry data
# TAKING THIS OUT FOR NOW- This data is highly correlated with the horse odds data

# d.entry <- d.entry %>% left_join(race.entry.df)

#d.entry <- race.entry.df %>% left_join(d.entry) # THIS IS TO TRY TO GET THE NEW DATA 

#d.entry <- d.entry %>% left_join(race.entry.df)


# TRANSFORM //putting in the track rebate information---------- ---------------------------------------------------------------------------

rebate.info.df.win <- rebate.info.df %>% filter(pool =='WIN') %>% 
  select(reward, location_code)

d.entry <- d.entry %>% left_join(rebate.info.df.win, by = c("track" = "location_code"))

d.entry$reward[is.na(d.entry$reward)] <- 0

# race.stuff.df.selected <- race.stuff.df %>% select(race_id, distance)
# d.entry <- d.entry %>% left_join(race.stuff.df.selected)
#  
 
# Write //end of ETL A----------------------------------------- ---------------------------------------------------------------------------

write.csv(d.entry, "_output_system/ultron_p1/phase_1.csv")



