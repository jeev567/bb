
build_ANN <- function(x_train_tbl,y_train_vec,v_test_tbl){

# Building our Artificial Neural Network
  model_keras <- keras_model_sequential()
  
  model_keras %>% 
    # First hidden layer
    layer_dense(
      units              = 256,  #64
      kernel_initializer = "uniform", 
      #kernel_regularizer = regularizer_l2(.001),
      activation         = "relu", 
      input_shape        = ncol(x_train_tbl)) %>% 
      layer_dropout(rate = 0.2) %>%  # .5
    
    
    # Second hidden layer
    layer_dense(
      units              = 128, 
      kernel_initializer = "uniform", 
      #kernel_regularizer = regularizer_l2(.001),
      activation         = "relu") %>% 
    layer_dropout(rate = 0.2) %>%
    
    # third hidden layer
    layer_dense(
      units              = 64,
      kernel_initializer = "uniform",
      #kernel_regularizer = regularizer_l2(.001),
      activation         = "relu") %>%
    layer_dropout(rate = 0.2) %>%

  
    # Output layer
    layer_dense(
      units              = 1, 
      kernel_initializer = "uniform", 
      activation         = "sigmoid") %>% 
    
    # Compile ANN
    compile(
      optimizer = 'rmsprop', #'adam
      loss      = 'binary_crossentropy',
      metrics   = c('accuracy')
    )
  
  keras_model
  
  history <- fit(
    object           = model_keras, 
    x                = as.matrix(x_train_tbl), 
    y                = y_train_vec,
    batch_size       = 1000, 
    epochs           = 30, #30
    validation_split = 0.15
  )
  
  # Predicted Class
  yhat_keras_class_vec <- predict_classes(object = model_keras, x = as.matrix(v_test_tbl)) %>%
    as.vector()
  
  # Predicted Class Probability
  yhat_keras_prob_vec  <- predict_proba(object = model_keras, x = as.matrix(v_test_tbl)) %>%
    as.vector()
  
  #install.packages("yardstick")
  library(yardstick)
  options(yardstick.event_first = FALSE)
  
  # come up with an ideal split
  
  #tested.cutoffs <- c(.1, .15, .2, .25, .3, .35, .4, .45, .5, .55, .6)
  tested.cutoffs <- .40
  tested.cuttoff.results <- data.frame(
    tested.cutoff = numeric(),
    precision = numeric(), 
    recall = numeric()
  )
  
  for (ii in tested.cutoffs){
  
  # Format test data and predictions for yardstick metrics
  estimates_keras_tbl <- tibble(
    truth      = as.factor(y_test_vec) %>% fct_recode(yes = "1", no = "0"),
    estimate   = as.factor(ifelse(yhat_keras_prob_vec >ii, "yes", "no")),#as.factor(yhat_keras_class_vec) %>% fct_recode(yes = "1", no = "0"),
    class_prob = yhat_keras_prob_vec
  )
  # estimates_keras_tbl
  # 
  # estimates_keras_tbl %>% conf_mat(truth, estimate)
  # 
  # estimates_keras_tbl %>% metrics(truth, estimate)
  # 
  # tibble(
  #   precision = estimates_keras_tbl %>% precision(truth, estimate),
  #   recall    = estimates_keras_tbl %>% recall(truth, estimate)
  # )
  
  temp.tested.cutoff.results <- data.frame(
    tested.cutoff = ii,
    precision = estimates_keras_tbl %>% precision(truth, estimate),
    recall    = estimates_keras_tbl %>% recall(truth, estimate)
    
  )
  
 tested.cuttoff.results <- rbind(temp.tested.cutoff.results, tested.cuttoff.results) 

 return(model_keras)
  
}
}


build_xgboost <- function(x_train_tbl,y_train_vec,v_test_tbl){
library(xgboost)
  
  params <- list(
    "objective"           =  "binary:logistic",
    "eval_metric"         = "logloss",#"rmse", #"map", #"rmse",#"map",#'error',# "logloss",
    "eta"                 =  .02, #.02, #.05, # trying degault rate#.05, #0.1,  adjusting learning rate
    "max_depth"           = 3,#6,
    "min_child_weight"    = 8, #10,
    "gamma"               = 0.70,
    "subsample"           = 0.80,
    "colsample_bytree"    = 0.95,
    "alpha"               = 2e-05,
    "lambda"              = 10,#,
    "max_delta_step"      = 1
    
    #"tree_method"         = "exact"
  )

    train <- x_train_tbl
    train$is_win <- y_train_vec

subtrain<- train
  X <- xgb.DMatrix(as.matrix(subtrain %>% select(-is_win,
                                                 -reward
                                                 #,
                                                 # -ranking,
                                                 # -market_prob,
                                                 # -horse_dollar_odds,
                                                 # -two.dollar.win,
                                                 # -leadertime,
                                                 # -leadertim2,
                                                 # -leadertim3,
                                                 # -leadertim4,
                                                 # -horsetime1,
                                                 # -horsetime2,
                                                 # -horsetimes,
                                                 # -horsetimef,
                                                 # -speed,
                                                 # -split1,
                                                 # -split2,
                                                 # -splits,
                                                 # -splitf,
                                                 # -leader_split1,
                                                 # -leader_split2,
                                                 # -leader_splits,
                                                 # -leader_splitf,
                                                 # -horsetime1speed,
                                                 # -horsetime2speed,
                                                 # -horsetimesspeed,
                                                 # -horsetimefspeed,
                                                 # -leader_horsetime1speed,
                                                 # -leader_horsetime2speed,
                                                 # -leader_horsetimesspeed,
                                                 # -leader_horsetimefspeed,
                                                 # -win_payoff,
                                                 # -place_payoff,
                                                 # -show_payoff,
                                                 # -show_payoff2
                                                 )), label = subtrain$is_win)
  model <- xgboost(data = X, params = params, nrounds = 800) #10000#2000
  
  importance <- xgb.importance(colnames(X), model = model)
  print(xgb.ggplot.importance(importance))
  
  return(model)

}


