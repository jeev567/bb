!#/bin/bash
rm ./_output_system/charts_merge/d.entry.merge.2.header.csv
rm ./_output_system/charts_merge/d.entry.merge.2.body.csv

head -1 `ls ./_output_system/charts/*.csv | head -1` > ./_output_system/charts_merge/d.entry.merge.2.header.csv

for filename in $(ls ./_output_system/charts/*.csv); do 
  echo $filename
  sed 1d $filename >> ./_output_system/charts_merge/d.entry.merge.2.body.csv; 
done

echo 'starting sort | uniq'
sort ./_output_system/charts_merge/d.entry.merge.2.body.csv | uniq > ./_output_system/charts_merge/d.entry.merge.2.body.sort.csv

rm ./_output_system/charts_merge/d.entry.merge.2.body.csv

cat ./_output_system/charts_merge/d.entry.merge.2.header.csv ./_output_system/charts_merge/d.entry.merge.2.body.sort.csv > ./_output_system/charts_merge/d.entry.merge.csv
rm ./_output_system/charts_merge/d.entry.merge.2.header.csv
rm ./_output_system/charts_merge/d.entry.merge.2.body.sort.csv
