# get recent data
cd ./_data_raw/
./run_get_data_for_n_days_back.sh -2 2
cd ..

# charts
./run_task_prepare_data_charts_v3.0.sh
./task_merge_charts_v2.0.sh
rm ./_output_system/charts_merge/*.zip
zip ./_output_system/charts_merge/d.entry.merge.csv.zip ./_output_system/charts_merge/d.entry.merge.csv
aws s3api put-object --bucket fotwoutput0001 --key _model_data/d.entry.merge.csv.zip --body ./_output_system/charts_merge/d.entry.merge.csv.zip 
# make public at: https://s3-us-west-2.amazonaws.com/fotwoutput0001/_model_data/d.entry.merge.csv.zip
#aws s3api put-object-acl --bucket fotwoutput0001 --key _model_data/d.entry.merge.csv.zip --grant-read uri=http://acs.amazonaws.com/groups/global/AllUsers
rm ./_output_system/charts_merge/*.zip

# past performance
./run_task_prepare_data_pp_v3.0.sh
rm ./_output_system/pp_merge_unique/*.csv

filetypelist=("sire" "dam" "trainer" "jockey" "race" "race_entry" "past_performance" "workout")
for filetype in ${filetypelist[@]}; do
  echo ${filetype}
  date
  head -1 ./_output_system/pp_merge/pp_${filetype}_merge.csv > ./_output_system/pp_merge/pp_${filetype}_merge_header.csv
  tail -n +2 ./_output_system/pp_merge/pp_${filetype}_merge.csv > ./_output_system/pp_merge/pp_${filetype}_merge_body.csv
  awk '/^"/ {if (f) print f; f=$0; next} {f=f FS $0} END {print f}' ./_output_system/pp_merge/pp_${filetype}_merge_body.csv > ./_output_system/pp_merge/pp_${filetype}_merge_body_u1.csv
  rm ./_output_system/pp_merge/pp_${filetype}_merge_body.csv
  sort ./_output_system/pp_merge/pp_${filetype}_merge_body_u1.csv | uniq > ./_output_system/pp_merge/pp_${filetype}_merge_body_u2.csv
  rm ./_output_system/pp_merge/pp_${filetype}_merge_body_u1.csv
  cat ./_output_system/pp_merge/pp_${filetype}_merge_header.csv ./_output_system/pp_merge/pp_${filetype}_merge_body_u2.csv > ./_output_system/pp_merge/pp_${filetype}_merge_full.csv
  rowcount=`head -1 ./_output_system/pp_merge/pp_${filetype}_merge_header.csv | sed 's/[^,]//g' | wc -c`
  awk -F',' -v i=`echo ${rowcount}` 'NF==i{print}' ./_output_system/pp_merge/pp_${filetype}_merge_full.csv  > ./_output_system/pp_merge_unique/pp_${filetype}_merge_unique.csv
  rm ./_output_system/pp_merge/pp_${filetype}_merge_header.csv
  rm ./_output_system/pp_merge/pp_${filetype}_merge_body_u2.csv
  rm ./_output_system/pp_merge/pp_${filetype}_merge_full.csv
done

date
zip ./_output_system/pp_merge_unique/pp_merge_unique.zip ./_output_system/pp_merge_unique/*.csv
date
aws s3api put-object --bucket fotwoutput0001 --key _model_data/pp_merge_unique.zip --body ./_output_system/pp_merge_unique/pp_merge_unique.zip 
# make public at: https://s3-us-west-2.amazonaws.com/fotwoutput0001/_model_data/d.entry.merge.csv.zip
#aws s3api put-object-acl --bucket fotwoutput0001 --key _model_data/pp_merge_unique.zip --grant-read uri=http://acs.amazonaws.com/groups/global/AllUsers
rm ./_output_system/pp_merge_unique/*.zip
