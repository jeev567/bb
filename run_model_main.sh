#!/bin/sh
cd "$(dirname "$0")"
source /home/aanalytics/.bash_profile

jobdate=`date +%Y%m%d`
logfilename=$jobdate'_model.log'

echo 'starting job' > ./_logs/$logfilename
echo `date` >> ./_logs/$logfilename

rm -f ./_output_system/charts_merge/*.*

aws s3api get-object --bucket 'fotwoutput0001' --key '_model_data/d.entry.merge.csv.zip' d.entry.merge.csv.zip

echo 'downloaded charts' >> ./_logs/$logfilename

unzip -o d.entry.merge.csv.zip
rm -f d.entry.merge.csv.zip

rm -f ./_output_system/pp_merge_unique/*.*
aws s3api get-object --bucket 'fotwoutput0001' --key '_model_data/pp_merge_unique.zip' pp_merge_unique.zip

echo 'downloaded past performance' >> ./_logs/$logfilename

unzip -o pp_merge_unique.zip
rm -f pp_merge_unique.zip

rm -f ./_output_system/ultron_p1/*.*
Rscript ultron_ETL_A.R

rm -f ./_output_system/ultron_p2/*.*
Rscript ultron_ETL_B.R

Rscript results_reporting.R

rm -f ./_output_system/ultron_model/*.*
Rscript ultron_model_Uc.R

aws s3api put-object --bucket 'fotwoutput0001' --key '_model_output/bet_ticket.csv' --body ./_output_system/ultron_model/bet_ticket.csv

aws s3api put-object --bucket 'fotwoutput0001' --key '_model_output/bet_ticket_results.csv' --body ./_output_user/bet_ticket_results.csv

echo `date`
echo 'posted results' >> ./_logs/$logfilename
