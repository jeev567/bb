## merge_charts_v1.0.R ##
rm(list = ls())

options(tibble.print_max = 50, tibble.width = Inf)
options(scipen = 999)

library(readr)
library(lubridate)

current.date <- Sys.Date() #- 1 

date.filter <- paste(year(current.date)
                     ,ifelse(month(current.date) < 10, "0", "")
                     ,month(current.date)
                     ,ifelse(day(current.date) < 10, "0", "")
                     ,day(current.date)
                     , sep="")

d.entry <- NULL
data.file.list <- list.files("./_output_system/charts/", '.csv') #pattern = date.filter)
for(curr.file.name in data.file.list){
  print(curr.file.name)
  if (curr.file.name %in% c("_old", "empty.txt")) {
    next
  }
  d.entry.curr <- read.csv(paste("./_output_system/charts/", curr.file.name, sep=""))
  d.entry <- rbind(d.entry, d.entry.curr)
  rm(d.entry.curr)
  d.entry <- unique(d.entry) 
}

#d.main <- read.csv("./_output_system/charts_merge/d.entry.merge.csv")
#
#n.rows.orig <- nrow(d.main)
#print(paste('number of rows in d.main before merge', n.rows.orig))
#
#print(paste('min date after before merge', min(as.character(d.main$race_date))))
#print(paste('max date after before merge', max(as.character(d.main$race_date))))
#
#d.main <- rbind(d.main, d.entry)
#n.rows.after.append <- nrow(d.main)
#print(paste('number of rows in d.main after rbind', n.rows.after.append))
#
#print(paste('min date after after rbind', min(as.character(d.main$race_date))))
#print(paste('max date after after rbind', max(as.character(d.main$race_date))))
#

##
d.main <- d.entry
##
n.rows.after.d.entry.assignment <- nrow(d.main)
print(paste('number of rows in d.main after d.entry assignment', n.rows.after.d.entry.assignment))

print(paste('min date after d.entry assignment', min(as.character(d.main$race_date))))
print(paste('max date after d.entry.assignment', max(as.character(d.main$race_date))))

rm(d.entry)

d.main <- unique(d.main)
n.rows.after.unique <- nrow(d.main)
print(paste('number of rows in d.main after unique', n.rows.after.unique))

print(paste('min date after unique', min(as.character(d.main$race_date))))
print(paste('max date after unique', max(as.character(d.main$race_date))))

#if(n.rows.after.append > n.rows.orig){
  write.table(d.main
              , "./_output_system/charts_merge/d.entry.merge.csv"
              , row.names = FALSE
              #, append=TRUE
              , col.names=TRUE
              , sep = ",")  
#} else {
#  print("possible error no additional rows")
#}
rm(d.main)

