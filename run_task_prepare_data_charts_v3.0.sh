# run_task_prepare_data_charts.sh
# Rscript task_prepare_data_charts_v3.0.R abt &

## reference: https://stackoverflow.com/questions/32464050/how-to-run-a-fixed-number-of-processes-in-a-loop
#cores=16  
#for item in $(cat ./_config_system/track_list.txt); do
#  echo $item
#  Rscript task_prepare_data_charts_v3.0.R $item &
#  
#  # Check how many background jobs there are, and if it
#  # is equal to the number of cores, wait for anyone to
#  # finish before continuing.
#  background=( $(jobs -p) )
#  if (( ${#background[@]} == cores )); then
#    wait -n
#  fi
#done

MAX_PROCESSES=2;
PROCESSES=0;

for item in $(cat ./_config_system/track_list.txt); do
  echo $item
  Rscript task_prepare_data_charts_v3.0.R $item &
  sleep 1
	let PROCESSES=PROCESSES+1
  if [ $((PROCESSES % MAX_PROCESSES)) -eq 0 ]; then
    echo "wait $PROCESSES"
    wait
  fi
done  
wait

exit 0;
